<?php

use Illuminate\Database\Seeder;

class SpecializationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Table with specializations
        $specializations = array('POZ', 'Okulista', 'Urolog', 'Kardiolog', 'Rehabilitacja');

        // Seeds for specializations
        foreach ($specializations as $spec){
            DB::table('specialization')->insert([
                'name' => $spec,
                'added_on' => date('Y-m-d')
            ]);
        }

    }
}
