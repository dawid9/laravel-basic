<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class userTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Array with doctors
        $exampleDoctors = array(
            "doctor1" => array( 'firstName'=>"Dominika", 'lastName' => "ZABLOCKA", 'phone' => 221784525, 'email' => 'dz@gmail.com', 'status' => 'Dostępny', 'PESEL' => 789456123, 'type' => 'doctor'),
            "doctor2" => array( 'firstName'=>"Dawid", 'lastName' => "CZYZ", 'phone' => 221784526, 'email' => 'dc@gmail.com', 'status' => 'Dostępny', 'PESEL' => 789456321, 'type' => 'doctor'),
            "doctor3" => array( 'firstName'=>"Michal", 'lastName' => "KUROS", 'phone' => 221784511, 'email' => 'mk@gmail.com', 'status' => 'Dostępny', 'PESEL' => 789456741, 'type' => 'doctor'),
            "doctor4" => array( 'firstName'=>"Arkadiusz", 'lastName' => "WONSZ", 'phone' => 221784852, 'email' => 'aw@gmail.com', 'status' => 'Dostępny', 'PESEL' => 789456361, 'type' => 'doctor'),
            "doctor5" => array( 'firstName'=>"Patrycja", 'lastName' => "MEDREK", 'phone' => 221784566, 'email' => 'pm@gmail.com', 'status' => 'Dostępny', 'PESEL' => 789456837, 'type' => 'doctor'),
        );

        // Array with patients
        $examplePatients = array(
            'patient1' => array('firstName'=>'Jan', 'lastName'=>'Nowak', 'phone'=>785123457, 'email'=>'nowak@gmail.com', 'PESEL'=> 12345678, 'type'=>'patient'),
            'patient2' => array('firstName'=>'Janusz', 'lastName'=>'Kowalski', 'phone'=>785125557, 'email'=>'janusz@gmail.com', 'PESEL'=> 12349978, 'type'=>'patient'),
            'patient3' => array('firstName'=>'Grazyna', 'lastName'=>'Kowalska', 'phone'=>996123457, 'email'=>'grazyna@gmail.com', 'PESEL'=> 69545678, 'type'=>'patient'),
            'patient4' => array('firstName'=>'Sebastian', 'lastName'=>'Osiedle', 'phone'=>785123666, 'email'=>'seba@gmail.com', 'PESEL'=> 56745678, 'type'=>'patient'),
            'patient5' => array('firstName'=>'Kamil', 'lastName'=>'TumoLec', 'phone'=>786923457, 'email'=>'legia@gmail.com', 'PESEL'=> 92345678, 'type'=>'patient')
        );

        // Seeder for doctors
        foreach ($exampleDoctors as $doctors => $doctor){
            DB::table('users')->insert([
                'firstName'=> $doctor['firstName'],
                'lastName'=> $doctor['lastName'],
                'phone'=> $doctor['phone'],
                'email' => $doctor['email'],
                'status'=> $doctor['status'],
                'PESEL'=> $doctor['PESEL'],
                'type'=> $doctor['type'],
                'added_on' => date('Y-m-d')
            ]);
        }

        // Seeder for patients
        foreach ($examplePatients as $patients => $patient){
            DB::table('users')->insert([
                'firstName'=> $patient['firstName'],
                'lastName'=> $patient['lastName'],
                'phone'=> $patient['phone'],
                'email' => $patient['email'],
                'status'=> '',
                'PESEL'=> $patient['PESEL'],
                'type'=> $patient['type'],
                'added_on' => date('Y-m-d')
            ]);
        }
    }
}
