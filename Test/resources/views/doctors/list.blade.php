@extends('template')

@section('navTitle')
    @if (isset($navTitle))
        {{$navTitle}}
    @endif
@endsection('navTitle')

@section('content')
<div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Imię i nazwisko</th>
            <th scope="col">Telefon</th>
            <th scope="col">Email</th>
            <th scope="col">Status</th>
        </tr>
        </thead>
        <tbody>
        @foreach($doctorsList as $doctor)

            @if ($doctor->status === "Dostępny")
                <tr>
                    <th scope="row">{{$doctor->id}}</th>
                    <td>
                        <a href="/doctors/{{$doctor->id}}"> {{$doctor->firstName}} {{$doctor->lastName}} </a>
                    </td>
                    <td>{{$doctor->phone}}</td>
                    <td>{{$doctor->email}}</td>
                    <td>{{$doctor->status}}</td>
                </tr>
            @endif

            @if ($doctor->status === "Niedostępny")
                <tr>
                    <th scope="row">{{$doctor->id}}</th>
                    <td>
                        <a href="/doctors/{{$doctor->id}}"> {{$doctor->firstName}} {{$doctor->lastName}} </a>
                    </td>
                    <td>{{$doctor->phone}}</td>
                    <td>{{$doctor->email}}</td>
                    <td style="color:red">{{$doctor->status}}</td>
                </tr>
            @endif

        @endforeach
        </tbody>
    </table>
</div>

@endsection('content')



