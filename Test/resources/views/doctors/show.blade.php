<!-- Main template -->
@extends('template')

<!-- Page title -->
@section('navTitle')
    @if (isset($navTitle))
        {{$navTitle}}
    @endif
@endsection('navTitle')

<!-- Page content -->
@section('content')

    <div class="card" style="width: 18rem;">
        <div class="card-header">
            {{$doctor->firstName}} {{$doctor->lastName}}
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">
                Telefon: {{$doctor->phone}}
            </li>
            <li class="list-group-item">
                e-mail: {{$doctor->email}}
            </li>
            <li class="list-group-item">
                Status: {{$doctor->status}}
            </li>
        </ul>
    </div>

@endsection('content')
