@extends('template')

@section('navTitle')
    @if (isset($navTitle))
        {{$navTitle}}
    @endif
@endsection('navTitle')

@section('content')
    <div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Specjalizacja</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>

@endsection('content')
