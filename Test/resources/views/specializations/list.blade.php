@extends('template')

@section('navTitle')
    @if (isset($navTitle))
        {{$navTitle}}
    @endif
@endsection('navTitle')

@section('content')
    <div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Specjalizacja</th>
            </tr>
            </thead>
            <tbody>
            @foreach($specializations as $spec)

                    <tr>
                        <th scope="row">{{$spec->id}}</th>
                        <td>{{$spec->name}}</td>
                    </tr>

            @endforeach
            </tbody>
        </table>
    </div>

@endsection('content')












