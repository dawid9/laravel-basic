<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    protected $table = "visits";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'doctor_id', 'patient_id', 'date'
    ];
}
