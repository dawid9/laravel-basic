<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Repositories\UserRepository;

class DoctorController extends Controller
{

    public function index(UserRepository $userRepo)
    {

// Odwołanie się do modelu User przez Repository
        $users = $userRepo->getAllDoctors();

// Standardowe odwołanie do modelu User
//        $users = User::where('type', 'doctor')
//                            ->orderBy('status', 'asc')
//                            ->get();

        return view('doctors.list',   ['doctorsList' => $users,
                                            'navTitle' => 'Lista Lekarzy']);
    }


    public function show(UserRepository $userRepo, $id)
    {
        $doctor = $userRepo->find($id);
//      $doctor = User::find($id);

        return view('doctors.show', ['doctor' => $doctor,
                                        'navTitle' => 'Dane lekarza']);
    }

    public function create(UserRepository $userRepo)
    {

        $userRepo->create([
            'firstName'=> "Dawid",
            'lastName'=> strtoupper("Czyz"),
            'phone'=> 799193337,
            'email' => 'dc@gmial.com',
            'status'=>'Dostępny',
            'PESEL'=> 930619043,
            'type'=> "doctor",
            'added_on' => date('Y-m-d H:i:s')
        ]);

//        User::create([
//            'firstName'=> "Dawid",
//            'lastName'=> strtoupper("Czyz"),
//            'phone'=> 799193337,
//            'email' => 'dc@gmial.com',
//            'status'=>'Dostępny',
//            'PESEL'=> 930619043,
//            'type'=> "doctor",
//            'added_on' => date('Y-m-d H:i:s')
//        ]);

        return redirect('doctors');
    }




    public function edit(UserRepository $userRepo, $id)
    {

          $userRepo->update($data, $id);

//        $doctor = User::find($id);
//        $doctor->firstName = "Dominika";
//        $doctor->lastName = "Zablocka";
//        $doctor->save();

        return redirect('doctors');
    }
}
