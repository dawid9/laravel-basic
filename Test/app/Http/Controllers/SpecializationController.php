<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Specialization;
use App\Repositories\SpecializationRepository;

class SpecializationController extends Controller
{
    public function index(SpecializationRepository $specRepo)
    {
        $specializations = $specRepo->getAll();

        return view('specializations.list',   ['specializations' => $specializations,
            'navTitle' => 'Specjalizacje']);
    }
}
